import { Component, OnInit } from '@angular/core';
import { character } from '../../model/character';
import { ActivatedRoute, Router } from '@angular/router';
import { CharacterService} from '../../service/character.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-update-character',
  templateUrl: './update-character.component.html',
  styleUrls: ['./update-character.component.css']
})
export class UpdateCharacterComponent implements OnInit {

  id: number;
  character: character;


  constructor(private route: ActivatedRoute, private router: Router,
    private CharacterService: CharacterService) { }

  ngOnInit() {
    this.character = new character();

    this.id = this.route.snapshot.params['id'];

    this.CharacterService.getCharacter(this.id)
    .subscribe(data => {
      console.log(data)
      this.character = data;
      }, error => console.log(error));
  }

  updateCharacter(){
    this.CharacterService.updateCharacter(this.id, this.character)
    .subscribe(data => {
      console.log(data);
      this.character = new character();
      this.gotoList();
    }, error => console.log(error));
  }

  onSubmit(){
    this.updateCharacter();
  }

  gotoList(){
    this.router.navigate(['/character-viewer'])
  }

  showMessage(){
    swal.fire({
      icon: 'success',
      title: 'Successfully updated',
      iconColor: '#d32f2f',
      confirmButtonColor: '#d32f2f'
    })
  }

}
