import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { character} from '../../model/character';
import { CharacterService } from '../../service/character.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-character-sheet',
  templateUrl: './character-sheet.component.html',
  styleUrls: ['./character-sheet.component.css']
})
export class CharacterSheetComponent implements OnInit {

  id: number;
  character: character;
  str: number;
  dex: number;
  con: number;
  inte: number;
  wis: number;
  cha: number;


  constructor(private route: ActivatedRoute, private router: Router,
    private CharacterService: CharacterService) { }

  ngOnInit() {
    this.character = new character();

    this.id = this.route.snapshot.params['id'];

    this.CharacterService.getCharacter(this.id)
    .subscribe(data => {
      console.log(data)
      this.character = data;
      this.str = this.character.str;
      this.dex = this.character.dex;
      this.con = this.character.con;
      this.inte = this.character.inte;
      this.wis = this.character.wis;
      this.cha = this.character.cha;
      }, error => console.log(error));

  }

  updateCharacter(){
    this.CharacterService.updateCharacter(this.id, this.character)
    .subscribe(data => {
      console.log(data);
      this.character = new character();
      this.gotoList();
    }, error => console.log(error));
  }

  onSubmit(){
    this.updateCharacter();
  }

  gotoList(){
    this.router.navigate(['/character-viewer'])
  }

  showMessage(){
    swal.fire({
      icon: 'success',
      title: 'Successfully updated',
      iconColor: '#d32f2f',
      confirmButtonColor: '#d32f2f'
    })
  }

  sacarMedia(num: number){
    console.log(num);
    switch (num) {
      case 1:
        return -5;
      case 2:
        return -4;
      case 3:
        return -4;
      case 4:
        return -3;
      case 5:
        return -3;
      case 6:
        return -2;
      case 7:
        return -2;
      case 8:
        return -1;
      case 9:
        return -1;
      case 10:
        return 0;
      case 11:
        return 0;
      case 12:
        return 1;
      case 13:
        return 1;
      case 14:
        return 2;
      case 15:
        return 2;
      case 16:
        return 3;
      case 17:
        return 3;
      case 18:
        return 4;
      case 19:
        return 4;
      case 20:
        return 5;
      case 21:
        return 5;
      case 22:
        return 6;
      case 23:
        return 6;
      case 24:
        return 7;
        case 25:
          return 7;
      case 26:
        return 8;
      case 27:
        return 8;
      case 28:
        return 9;
      case 29:
        return 9;
      default:
        return 10;
    }
  }

}
