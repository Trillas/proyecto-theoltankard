import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { character} from '../../model/character';
import { CharacterService } from '../../service/character.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-character-viewer',
  templateUrl: './character-viewer.component.html',
  styleUrls: ['./character-viewer.component.css']
})
export class CharacterViewerComponent implements OnInit {

  character: Observable<character[]>;

  constructor(private CharacterService: CharacterService,
    private router: Router) { }

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.character = this.CharacterService.getCharacterList();
  }

  deleteCharacter(id: number) {
    this.CharacterService.deleteCharacter(id)
    .subscribe(
      data => {
        console.log(data);
        this.reloadData();
      },
      error => console.log(error));
  }

  updateCharacter(id:number) {
    this.router.navigate(['character-update', id]);
  }

  showMessage(){
    swal.fire({
      icon: 'success',
      title: 'Successfully deleted',
      iconColor: '#d32f2f',
      confirmButtonColor: '#d32f2f'
    })
  }

  gotoCreate(){
    this.router.navigate(['character-creation'])
  }

  gotoSheet(id:number){
    this.router.navigate(['character-sheet', id])
  }

  confirmacionDelete(id:number){
    swal.fire({
      title: 'Do you want to delete the Character?',
      showDenyButton: true,
      confirmButtonText: 'Delete',
      denyButtonText:'Cancel',
      confirmButtonColor: '#d32f2f',
      denyButtonColor: '#d32f2f',
    }).then((result) => {
      if (result.isConfirmed) {
        this.deleteCharacter(id);
        swal.fire({
          icon: 'success',
          title: 'Successfully deleted',
          iconColor: '#d32f2f',
          confirmButtonColor: '#d32f2f'
        })
      } else if (result.isDenied) {

      }
    })
   }

}
