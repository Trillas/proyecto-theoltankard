import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateNotesJugadorComponent } from './create-notes-jugador.component';

describe('CreateNotesJugadorComponent', () => {
  let component: CreateNotesJugadorComponent;
  let fixture: ComponentFixture<CreateNotesJugadorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateNotesJugadorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateNotesJugadorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
