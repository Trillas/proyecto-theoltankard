import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { notesJugador } from '../../model/notes-jugador';
import { NotesJugadorService } from '../../service/notes-jugador.service';

@Component({
  selector: 'app-create-notes-jugador',
  templateUrl: './create-notes-jugador.component.html',
  styleUrls: ['./create-notes-jugador.component.css']
})
export class CreateNotesJugadorComponent implements OnInit {

  notes: notesJugador = new notesJugador;
  submitted = false;

  constructor(private  NotesJugadorService:  NotesJugadorService,
    private router: Router) { }

  ngOnInit(): void {

  }

  newNotes(): void{
    this.submitted = false;
    this.notes = new notesJugador;
  }

  save(){
    this.NotesJugadorService.createNotes(this.notes).subscribe(data => {
      console.log(data)
      this.notes = new notesJugador();
      this.gotoList();
    },
    error => console.log(error));
  }

  onSubmit(){
    this.submitted = true;
    this.save();
  }

  gotoList(){
    this.router.navigate(['/listnotesJugador']);
  }

  showMessage(){
    swal.fire({
      icon: 'success',
      title: 'Successfully created',
      iconColor: '#d32f2f',
      confirmButtonColor: '#d32f2f'
    })
  }
}
