import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListNotesJugadorComponent } from './list-notes-jugador.component';

describe('ListNotesJugadorComponent', () => {
  let component: ListNotesJugadorComponent;
  let fixture: ComponentFixture<ListNotesJugadorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListNotesJugadorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListNotesJugadorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
