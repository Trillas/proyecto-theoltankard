import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateNotesJugadorComponent } from './update-notes-jugador.component';

describe('UpdateNotesJugadorComponent', () => {
  let component: UpdateNotesJugadorComponent;
  let fixture: ComponentFixture<UpdateNotesJugadorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateNotesJugadorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateNotesJugadorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
