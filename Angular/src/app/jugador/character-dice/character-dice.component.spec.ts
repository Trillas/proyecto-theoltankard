import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CharacterDiceComponent } from './character-dice.component';

describe('CharacterDiceComponent', () => {
  let component: CharacterDiceComponent;
  let fixture: ComponentFixture<CharacterDiceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CharacterDiceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CharacterDiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
