import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MonsterDiceComponent } from './monster-dice.component';

describe('MonsterDiceComponent', () => {
  let component: MonsterDiceComponent;
  let fixture: ComponentFixture<MonsterDiceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MonsterDiceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MonsterDiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
