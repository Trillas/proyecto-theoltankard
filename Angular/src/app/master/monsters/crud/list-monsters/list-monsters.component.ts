import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { monsters } from '../../model/monsters';
import { MonstersService } from '../../service/monsters.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-list-monsters',
  templateUrl: './list-monsters.component.html',
  styleUrls: ['./list-monsters.component.css']
})
export class ListMonstersComponent implements OnInit {

  monsters: Observable<monsters[]>;

  constructor(private MonstersService: MonstersService,
    private router: Router) { }

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.monsters = this.MonstersService.getMonsterList();
  }

  deleteMonsters(id: number) {
    this.MonstersService.deleteMonster(id)
    .subscribe(
      data => {
        console.log(data);
        this.reloadData();
      },
      error => console.log(error));
  }

  updateMonsters(id:number) {
    this.router.navigate(['updatemonsters', id]);
  }

  showMessage(){
    swal.fire({
      icon: 'success',
      title: 'Successfully deleted',
      iconColor: '#d32f2f',
      confirmButtonColor: '#d32f2f'
    })
  }

  gotoCreate(){
    this.router.navigate(['createmonsters'])
  }

  confirmacionDelete(id:number){
    swal.fire({
      title: 'Do you want to delete the monster?',
      showDenyButton: true,
      confirmButtonText: 'Delete',
      denyButtonText:'Cancel',
      confirmButtonColor: '#d32f2f',
      denyButtonColor: '#d32f2f',
    }).then((result) => {
      if (result.isConfirmed) {
        this.deleteMonsters(id);
        swal.fire({
          icon: 'success',
          title: 'Successfully deleted',
          iconColor: '#d32f2f',
          confirmButtonColor: '#d32f2f'
        })
      } else if (result.isDenied) {

      }
    })
   }


}


