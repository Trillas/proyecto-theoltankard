import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { monsters } from '../../model/monsters';
import { MonstersService} from '../../service/monsters.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-create-monsters',
  templateUrl: './create-monsters.component.html',
  styleUrls: ['./create-monsters.component.css']
})
export class CreateMonstersComponent implements OnInit {

  monsters: monsters = new monsters;
  submitted = false;

  constructor(private MonstersService: MonstersService,
    private router: Router) { }

  ngOnInit(): void {
  }

  newMonster(): void{
    this.submitted = false;
    this.monsters = new monsters;
  }

  save() {
    this.MonstersService.createMonster(this.monsters).subscribe(data => {
      console.log(data);
      this.monsters = new monsters();
      this.gotoList();
    },
    error => console.log(error));
  }

  onSubmit(){
    this.submitted = true;
    this.save();
  }

  gotoList(){
    this.router.navigate(['/character-viewer']);
  }

  showMessage(){
    swal.fire({
      icon: 'success',
      title: 'Successfully created',
      iconColor: '#d32f2f',
      confirmButtonColor: '#d32f2f'
    })
  }
}
