import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateMonstersComponent } from './create-monsters.component';

describe('CreateMonstersComponent', () => {
  let component: CreateMonstersComponent;
  let fixture: ComponentFixture<CreateMonstersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateMonstersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateMonstersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
