import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateMonstersComponent } from './update-monsters.component';

describe('UpdateMonstersComponent', () => {
  let component: UpdateMonstersComponent;
  let fixture: ComponentFixture<UpdateMonstersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateMonstersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateMonstersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
