import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MonstersService {

  private baseUrl = 'http://localhost:3306/api/monsters'

  constructor(private http: HttpClient) { }

  getMonster(id:number) : Observable<any>{
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  createMonster(monsters: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}`, monsters);
  }

  updateMonster(id: number, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${id}`, value);
  }

  deleteMonster(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
  }

  getMonsterList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }
}
