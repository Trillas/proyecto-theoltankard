import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menuprincipal-master',
  templateUrl: './menuprincipal-master.component.html',
  styleUrls: ['./menuprincipal-master.component.css']
})
export class MenuprincipalMasterComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  activeClass(){
    $('#triggered').attr('class', 'img-thumbnail');
    $('#triggeredtexto').attr('class', ' ');
  }

  imagechanger1(){
    $('#triggered').attr('src', '../../../assets/img/snowmonster.jpg');
    $('#triggeredtexto').text("Create a new monster as you want!");
  }

  imagechanger2(){
    $('#triggered').attr('src', '../../../assets/img/dragons.jpg');
    $('#triggeredtexto').text("Look at the monsters you have created!");
  }

  imagechanger3(){
    $('#triggered').attr('src', '../../../assets/img/dices.jpg');
    $('#triggeredtexto').text("Let's go find that Roll20!");
  }

  imagechanger4(){
    $('#triggered').attr('src', '../../../assets/img/notes.jpeg');
    $('#triggeredtexto').text("Write everything you need in notes!");
  }

  imagechanger5(){
    $('#triggered').attr('src', '../../../assets/img/backdoorRedim.jpg');
    $('#triggeredtexto').text("Go back to the main menu!");
  }

}
