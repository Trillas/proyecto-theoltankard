import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuprincipalMasterComponent } from './menuprincipal-master.component';

describe('MenuprincipalMasterComponent', () => {
  let component: MenuprincipalMasterComponent;
  let fixture: ComponentFixture<MenuprincipalMasterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MenuprincipalMasterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuprincipalMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
