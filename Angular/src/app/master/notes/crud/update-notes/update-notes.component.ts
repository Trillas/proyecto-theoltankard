import { Component, OnInit } from '@angular/core';
import swal from 'sweetalert2';
import { ActivatedRoute, Router } from '@angular/router';
import { notes } from '../../model/notes';
import { NotesService } from '../../service/notes.service';

@Component({
  selector: 'app-update-notes',
  templateUrl: './update-notes.component.html',
  styleUrls: ['./update-notes.component.css']
})
export class UpdateNotesComponent implements OnInit {

  id:number;

  notes: notes;

  constructor(private route: ActivatedRoute, private router: Router,
    private NotesService: NotesService) { }

  ngOnInit(){
    this.notes = new notes();

    this.id = this.route.snapshot.params['id'];

    this.NotesService.getNotes(this.id)
    .subscribe(data => {
      console.log(data)
      this.notes = data;
      }, error => console.log(error));
  }

  updateNotes(){
    this.NotesService.updateNotes(this.id, this.notes)
    .subscribe(data => {
      console.log(data);
      this.notes = new notes();
      this.gotoList();
    }, error => console.log(error));
  }

  onSubmit(){
    this.updateNotes();
  }

  gotoList(){
    this.router.navigate(['/listnotesMaster'])
  }

  showMessage(){
    swal.fire({
      icon: 'success',
      title: 'Successfully updated',
      iconColor: '#d32f2f',
      confirmButtonColor: '#d32f2f'
    })
  }
}
