import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { notes } from '../../model/notes';
import { NotesService } from '../../service/notes.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-list-notes',
  templateUrl: './list-notes.component.html',
  styleUrls: ['./list-notes.component.css']
})
export class ListNotesComponent implements OnInit {

  notes: Observable<notes[]>;

  constructor(private NotesService: NotesService,
    private router: Router) { }

  ngOnInit() {
     this.reloadData();
  }

   reloadData() {
     this.notes = this.NotesService.getNotesList();
  }

  deleteNotes(id: number) {
    this.NotesService.deleteNotes(id)
    .subscribe(
      data => {
        console.log(data);
        this.reloadData();
      },
      error => console.log(error));
  }

  updateNotes(id:number) {
    this.router.navigate(['updatenotesMaster', id]);
  }

  createNotes(){
    this.router.navigate(['createnotesMaster'])
  }

  stoppropagation(){
    event.stopPropagation();
  }

  confirmacionDelete(id:number){
    swal.fire({
      title: 'Do you want to delete the note?',
      showDenyButton: true,
      confirmButtonText: 'Delete',
      denyButtonText:'Cancel',
      confirmButtonColor: '#d32f2f',
      denyButtonColor: '#d32f2f',
    }).then((result) => {
      if (result.isConfirmed) {
        this.deleteNotes(id);
        swal.fire({
          icon: 'success',
          title: 'Successfully deleted',
          iconColor: '#d32f2f',
          confirmButtonColor: '#d32f2f'
        })
      } else if (result.isDenied) {

      }
    })
   }

}
