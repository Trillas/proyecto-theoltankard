import { Component, OnInit } from '@angular/core';
import swal from 'sweetalert2';
import { notes } from '../../model/notes';
import { NotesService } from '../../service/notes.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-notes',
  templateUrl: './create-notes.component.html',
  styleUrls: ['./create-notes.component.css']
})
export class CreateNotesComponent implements OnInit {

  notes: notes = new notes;
  submitted = false;

  constructor(private NotesService: NotesService,
  private router: Router) { }

  ngOnInit(): void {
  }

  newNotes(): void{
    this.submitted = false;
    this.notes = new notes;
  }

  save(){
    this.NotesService.createNotes(this.notes).subscribe(data => {
      console.log(data)
      this.notes = new notes();
      this.gotoList();
    },
    error => console.log(error));
  }

  onSubmit(){
    this.submitted = true;
    this.save();
  }

  gotoList(){
    this.router.navigate(['/listnotesMaster']);
  }

  showMessage(){
    swal.fire({
      icon: 'success',
      title: 'Successfully created',
      iconColor: '#d32f2f',
      confirmButtonColor: '#d32f2f'
    })
  }

}
